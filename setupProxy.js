const { createProxyMiddleware } = require("http-proxy-middleware");

//all the calls starting from /api/v1 would be forwarding to the backend server
module.exports = function (app) {
  app.use(
    "/api/v1",
    createProxyMiddleware({
      target: "http://localhost:9080",
      changeOrigin: true,
    })
  );
};
