import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import MainLayout from "./component/Layout/main_layout";
import Register from "./component/Authentication/register";
import Login from "./component/Authentication/login";
import Admin from "./component/Admin/admin";
import { Redirect } from "react-router-dom";

export const AuthContext = React.createContext();

const initialState = {
  isAuthenticated: false,
  user: null,
  token: null,
  successMesage: "",
};
const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
      localStorage.setItem("token", JSON.stringify(action.payload.data.data));
      localStorage.setItem("user", JSON.stringify(action.payload.data.extra));
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.data.extra,
        token: action.payload.data.data,
      };

    case "REGISTER":
      return {
        ...state,
        successMesage: action.payload,
      };
    case "LOGOUT":
      localStorage.clear();
      <Redirect to="/" />;
      return {
        ...state,
        isAuthenticated: false,
        user: null,
      };
    default:
      return state;
  }
};
const App = () => {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  const handleclick = (e) => {
    dispatch({
      type: "LOGOUT",
    });
  };
  return (
    <>
      <Router>
        <Switch>
          <MainLayout>
            {state.isAuthenticated && (
              <button className="logout" onClick={handleclick}>
                Logout
              </button>
            )}
            <AuthContext.Provider
              value={{
                state,
                dispatch,
              }}
            >
              <Route path="/login">
                <Login />
              </Route>
              {state.isAuthenticated && (
                <Route path="/register">
                  <Register />
                </Route>
              )}
              {state.isAuthenticated && (
                <Route path="/admin">
                  <Admin />
                </Route>
              )}
              <Redirect to="/" />
            </AuthContext.Provider>
          </MainLayout>
        </Switch>
      </Router>
    </>
  );
};

export default App;
