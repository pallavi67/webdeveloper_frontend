import React from "react";
import "./style.css";
import { AuthContext } from "../../App";
import axios from "axios";
import { useHistory } from "react-router-dom";
//https://www.freecodecamp.org/news/state-management-with-react-hooks/
let isInitial = true;

const Login = () => {
  const { dispatch } = React.useContext(AuthContext);
  const initialState = {
    email: "",
    password: "",
    role: "1",
    isSubmitting: false,
    errorMessage: null,
  };
  const [formData, setformData] = React.useState(initialState);
  let history = useHistory();
  const handlechange = (e) => {
    setformData({ ...formData, [e.target.name]: e.target.value });
  };

  React.useEffect(() => {
    if (isInitial) {
      isInitial = false;
      return;
    }
  }, []);
  const handleSubmit = async (e) => {
    e.preventDefault();

    setformData({
      ...formData,
      isSubmitting: true,
      errorMessage: null,
    });
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      let response = await axios.post("/api/v1/login", formData, config);
      console.log(response);
      dispatch({
        type: "LOGIN",
        payload: response,
      });

      setformData({
        ...formData,
        isSubmitting: false,
      });

      return history.replace("/admin");
    } catch (error) {
      setformData({
        ...formData,
        isSubmitting: false,
        errorMessage: error.message || error.statusText,
      });
    }
  };
  return (
    <div>
      <>
        <form id="form-id" onSubmit={handleSubmit}>
          <div className="flex-items">
            <div className="form_group">
              <input
                type="text"
                id="form-field"
                name="email"
                placeholder="email"
                value={formData.email || ""}
                required
                onChange={handlechange}
              ></input>
            </div>
            <div className="form_group">
              <input
                type="text"
                id="form-field"
                name="password"
                placeholder="password"
                value={formData.password || ""}
                required
                onChange={handlechange}
              ></input>
            </div>
          </div>
          {formData.errorMessage && (
            <span className="form-error">{formData.errorMessage}</span>
          )}
          <button className="login_btn" disabled={formData.isSubmitting}>
            {formData.isSubmitting ? "Loading..." : "Login"}
          </button>
        </form>
      </>
    </div>
  );
};

export default Login;
