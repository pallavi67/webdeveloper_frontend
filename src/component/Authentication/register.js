import React from "react";
import "./style.css";
import axios from "axios";
let isInitial = true;
const Register = () => {
  const [formData, setformData] = React.useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    role: "1",
  });

  const [message, setmessage] = React.useState("");
  const [error, seterror] = React.useState("");
  const handlechange = (e) => {
    setformData({ ...formData, [e.target.name]: e.target.value });
  };

  React.useEffect(() => {
    if (isInitial) {
      isInitial = false;
      return;
    }
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      let response = await axios.post("/api/v1/add_new_user", formData, config);
      console.log(response);
      if (response.status === "ok") {
        setmessage(response.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <>
        <form id="form-id" onSubmit={handleSubmit}>
          <div className="flex-items">
            <div className="form_group">
              <label>firstName</label>
              <input
                type="text"
                id="form-field"
                name="firstName"
                value={formData.firstName || ""}
                required
                onChange={handlechange}
              ></input>
            </div>
            <div className="form_group">
              <label>lastName</label>
              <input
                type="text"
                id="form-field"
                name="lastName"
                value={formData.lastName || ""}
                required
                onChange={handlechange}
              ></input>
            </div>

            <div className="form_group">
              <label>email</label>
              <input
                type="text"
                id="form-field"
                name="email"
                value={formData.email || ""}
                required
                onChange={handlechange}
              ></input>
            </div>

            <div className="form_group">
              <label>password</label>
              <input
                type="text"
                id="form-field"
                name="password"
                value={formData.password || ""}
                required
                onChange={handlechange}
              ></input>
            </div>
          </div>
          <input type="submit" className="favorite styled" />
        </form>
      </>
    </div>
  );
};

export default Register;
