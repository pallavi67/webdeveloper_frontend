import React from "react";

import Navbar from "./nav_bar";
import Footer from "../Authentication/footer";
const MainLayout = ({ children }) => {
  return (
    <div>
      <Navbar />
      {children}

      <Footer />
    </div>
  );
};

export default MainLayout;
