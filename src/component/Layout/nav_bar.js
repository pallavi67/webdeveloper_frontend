import React from "react";
import { Nav, NavLink, NavMenu, NavBtn, NavBtnLink } from "./navitems";
import "./style.css";
const Navbar = () => {
  return (
    <div>
      <Nav>
        <NavMenu>
          <NavLink to="/">Home</NavLink>
          <NavLink to="/about">About</NavLink>
          <NavLink to="/contact">Contact</NavLink>
          <NavLink to="/services">Services</NavLink>
          <NavLink to="/portfolio">portfolio</NavLink>
          <NavLink to="/blog">Blog</NavLink>
        </NavMenu>
        <NavBtn>
          <NavBtnLink to="/login">Sign In</NavBtnLink>
          <NavBtnLink to="/register">Sign Up</NavBtnLink>
        </NavBtn>
      </Nav>
      {/* <Sidebar /> */}
    </div>
  );
};

export default Navbar;
