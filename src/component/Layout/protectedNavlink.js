import React from "react";
import { Nav, NavLink, NavMenu, NavBtn } from "./navitems";
import { useHistory } from "react-router-dom";
import "./style.css";
const AdminNavbar = () => {
  let history = useHistory();
  const logout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    history.replace("/login");
  };
  return (
    <div>
      <Nav>
        <NavMenu>
          <NavLink to="/create">create Blog</NavLink>
          <NavLink to="/operate">operate Blog</NavLink>
          <NavLink to="/create-services">create services</NavLink>
          <NavLink to="/update-services">update services</NavLink>
          <NavLink to="/create-portfolio">create portfolio</NavLink>
          <NavLink to="/update-portfolio">update portfolio</NavLink>
          <NavLink to="/create-categories">create categories</NavLink>
          <NavLink to="/update-categories">update categories</NavLink>
        </NavMenu>
        <NavBtn className="inner_button" onClick={logout}>
          Signout
        </NavBtn>
      </Nav>
      {/* <Sidebar /> */}
    </div>
  );
};

export default AdminNavbar;
