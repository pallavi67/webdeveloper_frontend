module.exports = {
  local: "http://localhost:3000/",
  prod: "https://webdevpro.herokuapp.com/",
  testing: "https://webdevpro-qa.herokuapp.com/",
};
